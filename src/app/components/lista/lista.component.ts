import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { ProductService } from '../../services/Product.service';

import { Product } from '../../models/Product';
@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {
  Products: Product[];
  editState: boolean = false;
  ProductToEdit: Product;
  encapsulation: ViewEncapsulation.None
  
  constructor(public ProductService: ProductService) { }

  ngOnInit() {
    this.ProductService.getProducts().subscribe(Products => {
      //console.log(Products);
      this.Products = Products;
    });
  }

  deleteProduct(event, Product) {
    const response = confirm('are you sure you want to delete?');
    if (response ) {
      this.ProductService.deleteProduct(Product);
      location.reload();
    }
    return;
  }

  editProduct(event, Product) {
    this.editState = !this.editState;
    this.ProductToEdit = Product;
    
  }

  updateProduct(Product) {
    this.ProductService.updateProduct(Product);
    this.ProductToEdit = null;
    this.editState = false;
  }
  display: boolean = false;

  showDialog() {
      this.display = true;
  }

}
